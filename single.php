<?php get_header() ?>
<?php
$categories = get_the_category();
$category = $categories[0];
?>
<section class="site-container">
    <div class="container-fluid">
        <div class="row">
            <section class="col-xs-12 col-sm-8 col-md-8 main-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="main-post-title">
                            <h1><?php the_title() ?></h1>
                        </div>
                        <ul class="breadcrumb">
                            <li>
                                <a href="<?php echo get_home_url() ?>"><?php echo __('Home', 'simple') ?></a>
                            </li>
                            <li>
                                <a href="<?php echo get_category_link($category->cat_ID) ?>"><?php echo $category->name ?></a>
                            </li>
                            <li>
                                <?php the_title() ?>
                            </li>
                        </ul>
                        <div class="main-post-share">
                            <div class="btn-group" role="group" aria-label="...">
                                <div class="fb-share-button" data-href="<?php echo get_the_permalink() ?>" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Chia sẻ</a></div>
                                <a href="https://twitter.com/share" class="twitter-share-button" data-show-count="false">Tweet</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                                <div class="a2a_kit a2a_default_style">
                                    <a class="a2a_button_google_plus_share"></a>
                                </div>
                                <script async src="http://static.addtoany.com/menu/page.js"></script>
                            </div>
                        </div>
                        <div class="main-post-content">
                            <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                                    <?php the_content() ?>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </section>
            <aside class="col-xs-12 col-sm-4 col-md-4 main-sidebar main-sidebar">
                <?php get_sidebar() ?>
            </aside>
        </div>
    </div>
</section>
<?php get_footer() ?>