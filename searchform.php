<form role="search" method="get" class="search-form" action="<?php esc_url(home_url('/'))?>">
    <label>
        <input type="search" class="search-field" placeholder="<?php echo __('Search any firmware', 'simple') ?>" value="<?php get_search_query()?>" name="s" />
    </label>
</form>