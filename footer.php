<footer id="site-footer">
    <div class="container-fluid">
        <p class="text-center">Copyright &copy; 2014 - <?php echo date('Y') ?> - <?php bloginfo('sitename') ?></p>
    </div>
</footer>
</section>
<?php wp_footer(); ?>
</body>
</html>