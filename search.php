<?php get_header('facebook') ?>
<?php
$categories = get_the_category();
?>
<section class="site-container">
    <div class="container-fluid">
        <div class="row">
            <section class="col-xs-12 col-sm-8 col-md-8 main-content">
                <ul class="breadcrumb">
                    <li>
                        <a href="<?php echo get_home_url() ?>"><?php echo __('Home', 'simple') ?></a>
                    </li>
                    <li><?php echo __('You searched for "' . get_search_query() . '"', 'simple') ?></li>
                </ul>
                <div class="row">
                    <?php if (isset($categories[0])): ?>
                        <?php $category = $categories[0]; ?>
                        <?php
                        $args = array(
                            'orderby' => 'name',
                            'category' => $category->cat_ID,
                            'paged' => ( get_query_var('paged') ? get_query_var('paged') : 0 ),
                        );
                        $query = new WP_Query($args);

                        $max_num_pages = $GLOBALS['wp_query']->max_num_pages;
                        if (have_posts()):
                            while (have_posts()): the_post();
                                ?>    
                                <article class="col-xs-6 col-sm-3 col-md-3 category-item">
                                    <div class="category-thumb">
                                        <a href="<?php the_permalink() ?>">
                                            <img src="<?php
                                            if (has_post_thumbnail()) {
                                                the_post_thumbnail_url();
                                            } else {
                                                echo get_stylesheet_directory_uri() . '/no_image.jpg';
                                            }
                                            ?>" alt="<?php the_title() ?>" />
                                        </a>
                                    </div>
                                    <div class="category-name">
                                        <a href="<?php the_permalink() ?>">
                                            <?php echo mb_strimwidth(get_the_title(), 0, 20, ''); ?>
                                        </a>
                                    </div>
                                </article>
                            <?php endwhile; ?> 
                        <?php endif; ?>
                        <?php simple_pagination($max_num_pages); ?>
                    <?php endif; ?>
                </div>
            </section>
            <aside class="col-xs-12 col-sm-4 col-md-4 main-sidebar">
                <?php get_sidebar('facebook') ?>
            </aside>
        </div>
    </div>
</section>
<?php get_footer() ?>