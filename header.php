<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmgp.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?> >
        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8&appId=313013762402564";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
        <section id="wrapper">
            <header id="site-header">
                <!--.top-header-->
                <div class="top-header">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6 header-logo">
                                <h1 class="text-uppercase"><a href="<?php echo home_url() ?>"><?php bloginfo('sitename') ?></a></h1>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 text-right header-search">
                                <?php get_search_form() ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!--./top-header-->
                <!--.bottom-header-->
                <div class="bottom-header">
                    <nav class="navbar navbar-default" role="navigation">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="<?php echo get_home_url() ?>">Home</a>
                        </div>
                        <div class="collapse navbar-collapse navbar-ex1-collapse">
                            <?php simple_menu('primary-menu'); ?>
                        </div>
                    </nav>
                </div>
                <!--./bottom-header-->
            </header>