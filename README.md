#**Supper simple theme for Thanh Duy GSM**#
##1. Overview#
This theme design for wordpress of Thanh Duy GSM
Facebook: https://www.facebook.com/duy.khuong.7771586#
##2. Require plugins:##
###2.1. Categories Images###
https://wordpress.org/plugins/categories-images/
###2.2. jQuery Updater###
https://wordpress.org/plugins/jquery-updater/
##3. Installation##
Appearance > Themes > Add new > Upload Theme > Choose file [supertheme.zip](/supertheme.zip) > Install Now > Activate
##4. Setup menu##
Appearance > Menus > Choose item in menus > Check Display location is Primary > Save Menu
##5. Setup facebook page in sidebar##
Change only code line number 6 in file [sidebar.php](/sidebar.php)
```
#!php
data-href="**[your-facebook-url]**"
```
##6. Setup advertise image###
Change file [sidebar.php](/sidebar.php)
```
#!php
<a href="**[your-link-advertise]**">
    <img src="**[your-link-image-advertise]**" 
         alt="**[your-title-advertise]**" 
         class="img-responsive ads"/>
</a>
```
##7. Custom content of a post ##
This theme use bootstrap CSS Framework, so you can go to http://getbootstrap.com/ to see components and make your post content more awesome.
##8. Change favicon##
You can change favicon of website by change file [favicon.ico](/favicon.ico)
This file must be formatted with name favicon.ico
##9. Some screenshot##
![Screenshot](/screenshots/1.png?raw=true "Screenshot")
![Screenshot](/screenshots/2.png?raw=true "Screenshot")
![Screenshot](/screenshots/3.png?raw=true "Screenshot")
![Screenshot](/screenshots/4.png?raw=true "Screenshot")