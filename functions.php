<?php

/**
 * Define some constants and variables
 * THEME_URL: get theme directory path
 * CORE: Get core directory path
 */
define('THEME_URL', get_stylesheet_directory());
define('CORE', THEME_URL . '/core');

/**
 * Require file /core/init.php
 */
require_once CORE . '/init.php';

/**
 * Setup content width for template
 */
if (!isset($content_width)) {
    $content_width = 1020;
}

/**
 * Set some feature of theme
 */
if (!function_exists('simple_theme_setup')) {

    function simple_theme_setup() {

        //Setup textdomain
        $languages_folder = THEME_URL . '/languages';
        load_theme_textdomain('simple', $languages_folder);
        //Add RSS link to <head>
        add_theme_support('automatic-feed-links');
        //Adding feature thumbnail post
        add_theme_support('post-thumbnails');
        //Add <title> tag in <head>
        add_theme_support('title-tag');

        //Add menu
        register_nav_menu('primary-menu', __('Primary Menu', 'simple'));
        $sidebar = array(
            'name' => __('Main sidebar', 'simple'),
            'id' => 'main-sidebar',
            'description' => __('Default sidebar', 'simple'),
            'class' => 'site-sidebar',
            'before_title' => '',
            'after_title' => ''
        );
        register_sidebar($sidebar);
    }

    add_action('init', 'simple_theme_setup');
}


/**
 * Setup menu for simple theme
 */
if (!function_exists('simple_menu')) {

    function simple_menu($menu) {
        $simple_menu = array(
            'theme_location' => $menu,
            'menu_class' => '',
            'items_wrap' => '<ul id="%1$s" class="nav navbar-nav">%3$s</ul>'
        );
        wp_nav_menu($simple_menu);
    }

}

/**
 * Setup stylesheet and javascript for simple theme
 */
if (!function_exists('simple_setup_stylesheet')) {

    function simple_setup_stylesheet() {
        //style.css
        wp_register_style('main-style', get_template_directory_uri() . '/style.css', 'all');
        wp_enqueue_style('main-style');
    }

    add_action('wp_enqueue_scripts', 'simple_setup_stylesheet');
}
/**
 * Register jquery
 */
if (!function_exists('simple_setup_jquery')) {

    function simple_setup_jquery() {
        //all.js
        wp_enqueue_script('main-jquery', get_template_directory_uri() . '/all.js', array('jquery'));
        wp_enqueue_script('main-jquery');
    }

    add_action('wp_enqueue_scripts', 'simple_setup_jquery');
}



/**
 * Setup search form for simple theme
 */
if (!function_exists('simple_setup_search')) {

    function simple_setup_search() {
        add_theme_support('html5', array('search-form'));
    }

    add_action('after_setup_theme', 'simple_setup_search');
}

/**
 * Pagination in simple theme
 */
if (!function_exists('simple_pagination')) {

    function simple_pagination($max_num_pages = 0) {

        /** Stop execution if there's only 1 page */
        if ($max_num_pages <= 1)
            return;
        $paged = get_query_var('paged') ? absint(get_query_var('paged')) : 1;

        /** Add current page to the array */
        if ($paged >= 1)
            $links[] = $paged;

        /** Add the pages around the current page to the array */
        if ($paged >= 3) {
            $links[] = $paged - 1;
            $links[] = $paged - 2;
        }

        if (( $paged + 2 ) <= $max_num_pages) {
            $links[] = $paged + 2;
            $links[] = $paged + 1;
        }
        echo '<div class="pagination"><ul>' . "\n";

        /** Previous Post Link */
        if (get_previous_posts_link())
            printf('<li>%s</li>' . "\n", get_previous_posts_link('&laquo;'));

        /** Link to first page, plus ellipses if necessary */
        if (!in_array(1, $links)) {
            $class = 1 == $paged ? ' class="active"' : '';
            printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link(1)), '1');
            if (!in_array(2, $links))
                echo '<li>…</li>';
        }

        /** Link to current page, plus 2 pages in either direction if necessary */
        sort($links);
        foreach ((array) $links as $link) {
            $class = $paged == $link ? ' class="active"' : '';
            printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link($link)), $link);
        }

        /** Link to last page, plus ellipses if necessary */
        if (!in_array($max_num_pages, $links)) {
            if (!in_array($max_num_pages - 1, $links))
                echo '<li>…</li>' . "\n";
            $class = $paged == $max_num_pages ? ' class="active"' : '';
            printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link($max_num_pages)), $max_num_pages);
        }

        /** Next Post Link */
        if (get_next_posts_link())
            printf('<li>%s</li>' . "\n", get_next_posts_link('&raquo;'));
        echo '</ul></div>' . "\n";
    }

}