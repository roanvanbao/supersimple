<?php get_header() ?>
<section class="site-container">
    <div class="container-fluid">
        <div class="row">
            <section class="col-xs-12 col-sm-8 col-md-8 main-content">
                <div class="row">
                    <?php
                    $args = array(
                        'orderby' => 'name',
                        'hide_empty' => 0,
                        'parent' => 0,
                    );
                    $categories = get_categories($args);
                    global $wp_query;
                    $total_posts = $wp_query->post_count;
                    if (count($categories) > 0):
                        foreach ($categories as $cat):
                            ?>
                            <article class="col-xs-6 col-sm-3 col-md-3 category-item">
                                <div class="category-thumb">
                                    <a href="<?php echo get_category_link($cat->cat_ID) ?>">
                                        <img src="<?php echo (z_taxonomy_image_url($cat->term_id) != '') ? z_taxonomy_image_url($cat->term_id) : get_stylesheet_directory_uri() . '/no_image.jpg' ?>" alt="<?php echo $cat->name ?>" />
                                    </a>
                                </div>
                                <div class="category-name">
                                    <a href="<?php echo get_category_link($cat->cat_ID) ?>">
                                        <?php echo mb_strimwidth( $cat->name , 0, 20, '' ) ?>
                                    </a>
                                </div>
                            </article>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </section>
            <aside class="col-xs-12 col-sm-4 col-md-4 main-sidebar">
                <?php get_sidebar() ?>
            </aside>
        </div>
    </div>
</section>
<?php get_footer() ?>